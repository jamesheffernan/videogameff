global.paused_count += 1

// Check if paused is odd
if global.paused_count % 2 == 1 {
	global.game_paused = true;
}

// Check if paused is even
if global.paused_count % 2 == 0 {
	global.game_paused = false;
}