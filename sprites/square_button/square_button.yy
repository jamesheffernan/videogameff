{
    "id": "1fd989b9-b414-4107-b7be-45ceb351e3dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "square_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db71ac87-730f-4abf-809c-0214e66b3d01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fd989b9-b414-4107-b7be-45ceb351e3dc",
            "compositeImage": {
                "id": "9559c7e1-b460-42ec-8450-a5d556f5d480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db71ac87-730f-4abf-809c-0214e66b3d01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0874dc83-dbc5-4e1a-b6e5-5193a0d4bc2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db71ac87-730f-4abf-809c-0214e66b3d01",
                    "LayerId": "a30aee59-7146-40e5-b4f0-b6be70cd3fe5"
                }
            ]
        },
        {
            "id": "eab5b895-317b-4569-9f9d-5214ed116cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fd989b9-b414-4107-b7be-45ceb351e3dc",
            "compositeImage": {
                "id": "4ae9937e-ad2c-4bc4-900b-12f4434ca013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab5b895-317b-4569-9f9d-5214ed116cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e3de078-c977-4d03-87a9-efa96ee7ccc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab5b895-317b-4569-9f9d-5214ed116cf6",
                    "LayerId": "a30aee59-7146-40e5-b4f0-b6be70cd3fe5"
                }
            ]
        },
        {
            "id": "13472504-e454-4122-ac80-54d0bd84eaed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fd989b9-b414-4107-b7be-45ceb351e3dc",
            "compositeImage": {
                "id": "8561cb3e-1768-4150-a8fe-f0a7e7a72d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13472504-e454-4122-ac80-54d0bd84eaed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7369417a-f338-4fd2-9dc4-ef990299171b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13472504-e454-4122-ac80-54d0bd84eaed",
                    "LayerId": "a30aee59-7146-40e5-b4f0-b6be70cd3fe5"
                }
            ]
        },
        {
            "id": "e8f53745-dd1a-4b2b-8ea9-011819f3d954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fd989b9-b414-4107-b7be-45ceb351e3dc",
            "compositeImage": {
                "id": "3fcb03c7-b52e-4ec6-b965-f5f35e053607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8f53745-dd1a-4b2b-8ea9-011819f3d954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8717772-c51c-44ed-b458-e00aebb24fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8f53745-dd1a-4b2b-8ea9-011819f3d954",
                    "LayerId": "a30aee59-7146-40e5-b4f0-b6be70cd3fe5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a30aee59-7146-40e5-b4f0-b6be70cd3fe5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fd989b9-b414-4107-b7be-45ceb351e3dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}