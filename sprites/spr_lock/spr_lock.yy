{
    "id": "8dc9f118-9a21-4797-aa72-fc42e6c48b6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 7,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae178678-13b4-4377-8e12-dc92e923fa1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dc9f118-9a21-4797-aa72-fc42e6c48b6a",
            "compositeImage": {
                "id": "fc864550-1000-4509-830e-d6e6f708c001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae178678-13b4-4377-8e12-dc92e923fa1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9eb43e-903b-4e72-9a52-b589ba75c50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae178678-13b4-4377-8e12-dc92e923fa1e",
                    "LayerId": "88427524-ee83-4332-b8bc-befb13a84fbd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "88427524-ee83-4332-b8bc-befb13a84fbd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dc9f118-9a21-4797-aa72-fc42e6c48b6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}