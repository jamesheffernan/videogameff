{
    "id": "f7246fd5-10ed-4ece-b20a-b91201dbfebe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_none",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "565f8d22-aaef-46aa-92c8-0764859ea3a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7246fd5-10ed-4ece-b20a-b91201dbfebe",
            "compositeImage": {
                "id": "d75557a8-af61-4e55-a69a-bffff9f71095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565f8d22-aaef-46aa-92c8-0764859ea3a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3744c9b-bd91-4ecc-87eb-65371fcf83e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565f8d22-aaef-46aa-92c8-0764859ea3a8",
                    "LayerId": "5f4f57ef-6cc5-4321-820e-e382cb6588de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5f4f57ef-6cc5-4321-820e-e382cb6588de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7246fd5-10ed-4ece-b20a-b91201dbfebe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}