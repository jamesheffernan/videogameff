{
    "id": "455ecf89-d780-46aa-b5e9-ccd0391c58df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pause_screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcc1e140-79e5-4f2b-b312-5c5cd7e19abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "455ecf89-d780-46aa-b5e9-ccd0391c58df",
            "compositeImage": {
                "id": "42cd80be-6b44-4f28-9942-ef054e03e6ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcc1e140-79e5-4f2b-b312-5c5cd7e19abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ca3adfb-9c48-499f-9beb-eb732af1a570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcc1e140-79e5-4f2b-b312-5c5cd7e19abd",
                    "LayerId": "39106c88-3d9c-4517-b974-b396d5df747a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 704,
    "layers": [
        {
            "id": "39106c88-3d9c-4517-b974-b396d5df747a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "455ecf89-d780-46aa-b5e9-ccd0391c58df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 352
}