{
    "id": "3a641fe7-5be2-40bf-bd7c-d82e0261c9d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2567fa80-f292-4b06-93d9-1fd191995503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a641fe7-5be2-40bf-bd7c-d82e0261c9d2",
            "compositeImage": {
                "id": "a2c8cecf-f5ed-4e55-997f-2ccd02d5e73c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2567fa80-f292-4b06-93d9-1fd191995503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2318feaa-395b-425b-9950-b0d2ef73e17a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2567fa80-f292-4b06-93d9-1fd191995503",
                    "LayerId": "2a598ccb-dfcd-47d6-842d-272adbcc6fe5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "2a598ccb-dfcd-47d6-842d-272adbcc6fe5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a641fe7-5be2-40bf-bd7c-d82e0261c9d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 28
}