{
    "id": "243dca35-f3ca-4de9-a9dc-ef4fe46f267d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tutorial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3fdf60d-7d5d-4a81-bf9a-2ddfab957585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243dca35-f3ca-4de9-a9dc-ef4fe46f267d",
            "compositeImage": {
                "id": "32bc9d0a-774e-426d-adb2-eb8c70b2516c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3fdf60d-7d5d-4a81-bf9a-2ddfab957585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59238028-e58b-4f62-b8fb-b7dce340008a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3fdf60d-7d5d-4a81-bf9a-2ddfab957585",
                    "LayerId": "d973d6de-00fb-448c-ad57-56fce62d1485"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d973d6de-00fb-448c-ad57-56fce62d1485",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "243dca35-f3ca-4de9-a9dc-ef4fe46f267d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}