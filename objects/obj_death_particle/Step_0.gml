if (counter == 60) {
	instance_destroy();
	part_emitter_destroy(death_particles, death_emitter);
	part_system_destroy(death_particles);
	part_type_destroy(death_particle);
}
else
    counter++;