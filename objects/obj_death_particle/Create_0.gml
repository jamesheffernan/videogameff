counter = 0
death_particles = part_system_create()
part_system_depth(death_particles, 1)

death_particle = part_type_create()
part_type_shape(death_particle, pt_shape_square)
part_type_orientation(death_particle, 0, 360, 0, 0, 1)
part_type_size(death_particle, 0.05, 0.25, 0, 0)
part_type_speed(death_particle, 2, 2.5, 0.05, 0)
part_type_direction(death_particle, 0, 360, 0, 4)
part_type_life(death_particle, 15, 20)
part_type_color1(death_particle, make_color_rgb(34,181,115))

death_emitter = part_emitter_create(death_particle)
part_emitter_region(death_particles, death_emitter, x-64, x+64, y+64, y-64, ps_shape_rectangle, ps_distr_linear)
part_emitter_burst(death_particles, death_emitter, death_particle, 20)