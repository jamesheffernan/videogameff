draw_sprite(spr_arrow, 0, 160, 160)

ini_open("datafiles/saveData.ini")

if ini_read_real("FinshedLevels", "00", 0) == 0 {
	draw_text(256 + 7, 256 + 9, "00");
	draw_sprite(spr_lock, 0, 384, 256)

} else if ini_read_real("FinishedLevels", "00", 0) != 0 {
	draw_set_color(make_color_rgb(34, 181, 115));
	draw_text(256 + 7, 256 + 9, "00");
	draw_set_color(c_white)
	
} else if ini_key_exists("FinishedLevels", "01") == false {
	draw_text(384 + 10, 256 + 9, "01");
	draw_sprite(spr_lock, 0, 512 + 4, 256 + 4)

} else if ini_key_exists("FinishedLevels", "01") {
	draw_set_color(make_color_rgb(34, 181, 115));
	draw_text(384 + 10, 256 + 9, "01");
	draw_set_color(c_white);
	draw_text(512 + 7,256 + 9, "02");
}