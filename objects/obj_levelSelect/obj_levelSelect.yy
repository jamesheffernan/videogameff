{
    "id": "23c96f6f-6371-4bd4-bf39-1e447519323f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_levelSelect",
    "eventList": [
        {
            "id": "f371e463-9100-440d-ac5a-672bee10ece3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "23c96f6f-6371-4bd4-bf39-1e447519323f"
        },
        {
            "id": "bb9c2fb0-f5dc-4acb-9499-b63cf873d4d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "23c96f6f-6371-4bd4-bf39-1e447519323f"
        },
        {
            "id": "3319d12c-8236-4e05-955a-6e7a51e06d96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "23c96f6f-6371-4bd4-bf39-1e447519323f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}