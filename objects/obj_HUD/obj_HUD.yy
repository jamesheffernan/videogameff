{
    "id": "84bfc7dd-2e9c-452f-a15b-2b20145922dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HUD",
    "eventList": [
        {
            "id": "24494467-c815-4f16-ab38-6b37080bb192",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84bfc7dd-2e9c-452f-a15b-2b20145922dd"
        },
        {
            "id": "45ad1337-3f52-4b94-aff0-b71707d9cb90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "84bfc7dd-2e9c-452f-a15b-2b20145922dd"
        },
        {
            "id": "4e2fdd13-d24a-4fb9-aca9-56b6a1d60b2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "84bfc7dd-2e9c-452f-a15b-2b20145922dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}