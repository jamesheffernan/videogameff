draw_set_font(global.t);
draw_set_color(c_white);

if global.SelectLevel == false {
	
	wx=device_mouse_x_to_gui(0);
	wy=device_mouse_y_to_gui(0);
	
	draw_sprite(square_button, 0, 128, 128);
	if point_in_rectangle(wx, wy, 128, 128, 192, 192) {
		draw_sprite(square_button, 1, 128, 128);
		if device_mouse_check_button_pressed(0, mb_left) {
			pause_game();
		}
	}
	
	if global.game_paused {
		
		draw_sprite(spr_pause_screen, 0, 960, 540)
		draw_sprite(long_button, 0, 960, 412)
		draw_sprite(long_button, 0, 960, 508)
		draw_sprite(long_button, 0, 960, 604)
		
		draw_text(960 - (continue_str_len/2), 412 - 24, "Continue");
		draw_text(960 - (settings_str_len/2), 508 - 24, "Settings");
		draw_text(960 - (quit_str_len/2), 604 - 24, "Quit")
		
		// Continue
		if point_in_rectangle(wx, wy, 960 - 128, 412 - 32, 960 + 128, 412 + 32) {
			draw_sprite(long_button, 1, 960, 412)
			if device_mouse_check_button_pressed(0, mb_left) {
				pause_game();
			}
		}
		
		if point_in_rectangle(wx, wy, 960 - 128, 508 - 32, 960 + 128, 508 + 32) {
			draw_sprite(long_button, 1, 960, 508)
			if device_mouse_check_button_pressed(0, mb_left) {
				// Code for settings goes here
			}
		}
		
		// Quit
		if point_in_rectangle(wx, wy, 960 - 128, 604 - 32, 960 + 128, 604 + 32) {
			draw_sprite(long_button, 1, 960, 604)
			if device_mouse_check_button_pressed(0, mb_left) {
				room_goto(1);
			}
		}
	}	
}

draw_set_font(global.t);
draw_set_color(c_white);

continue_str_len = string_width("Continue");
settings_str_len = string_width("Settings");
quit_str_len = string_width("Quit");
split_string_length = string_width("Split Mode = ON")
split1_string_length = string_width("Split Mode = OFF")
draw_text(128 + 13, 128 + 8, "II" );

if global.toggleSplit == true {
	draw_text(1920 - split_string_length - 128, 136, "Split Mode = ON")
} else if global.toggleSplit == false {
	draw_text(1920 - split1_string_length - 128, 136, "Split Mode = OFF")
}

if global.finishedLevel == true {
	draw_text(960, 540, "You win!")
}