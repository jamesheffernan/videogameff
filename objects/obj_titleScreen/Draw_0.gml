/// @description Insert description here
// You can write your code in this editor
draw_set_font(global.t);
draw_set_color(c_white);

var str_width_1 = string_width("Asterix");
var str_width_2 = string_width("Play");
var str_width_3 = string_width("Settings");
var str_width_4 = string_width("Quit");

draw_text_transformed(960 - ((str_width_1 * 2)/2),296,"Asterix", 2, 2, 0);
draw_text(960 - (str_width_2/2),550, "Play");
draw_text(960 - (str_width_3/2),680, "Settings");
draw_text(960 - (str_width_4/2), 808, "Quit");
