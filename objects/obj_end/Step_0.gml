if position_meeting(x, y+1, obj_player) && global.finishedLevel != true {
	global.finishedLevel = true;
	room_goto(2);
}

if global.finishedLevel {
	if global.currentLevel == 0 {
		ini_open("datafiles/saveData.ini");
		ini_write_real("FinishedLevels", "00", 1)
		ini_close();
	} else if global.currentLevel == 1 {
		ini_open("datafiles/saveData.ini");
		ini_write_real("FinishedLevels", "01", 1)
		ini_close();
	} else if global.currentLevel == 2 {
		ini_open("datafiles/saveData.ini");
		ini_write_real("FinishedLevels", "02", 1)
		ini_close();
	}
}