if global.SelectLevel == true {
	if id == global.back_button.id {
		room_goto(0);
	} else if id == global.lvl0_button.id {
		room_goto(3);
	} else if id == global.lvl01_button.id && ini_read_real("FinishedLevels", "00", 0) != 0 {
		room_goto(4);
	} else if id == global.lvl02_button.id {
		room_goto(5);
	}
} else {
	pause_game()
}
