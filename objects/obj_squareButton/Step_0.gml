nearest_button = instance_nearest(mouse_x, mouse_y, obj_squareButton)

if position_meeting(mouse_x, mouse_y, nearest_button) {
	if nearest_button.id == global.lvl0_button && ini_read_real("FinishedLevels", "00", 0) != 0 {
		nearest_button.image_index = 3
	} else if nearest_button.id == global.lvl01_button && ini_read_real("FinishedLevels", "01", 0) != 0 {
		nearest_button.image_index = 3
	} else {
		nearest_button.image_index = 1
	}
} else if id == global.lvl0_button && ini_read_real("FinishedLevels", "00", 0) != 0 {
	image_index = 2
} else if id == global.lvl01_button && ini_read_real("FinishedLevels", "01", 0) != 0 {
	image_index = 2
} else {
	image_index = 0
}
